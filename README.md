# events.eclipse.org

The [events.eclipse.org](https://events.eclipse.org) website is generated with [Hugo](https://gohugo.io/documentation/).

The site provides date, time and place for the various events (conferences, demo camps, special days, hackathons and trainings) for the Eclipse ecosystem, displayed on a convenient map of the world.

## Getting started

### Required Software

| Software | Version |
|---------|---------|
| node.js | 18.13.0 |
| npm | 8.19 |
| Hugo | 0.110 |
| Git | > 2.31 |

See our [Managing Required Software](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/-/wikis/Managing-Required-Software) wiki page for more information on this topic.

Install dependencies, build assets and start a webserver:

```bash
yarn
hugo server
```

## Request an Event Website

Missing your event on this site? Please submit your event to the [Eclipse Newsroom](https://newsroom.eclipse.org/node/add/events).

Eclipse Foundation Staff can request an Event website similar to [Open Source AI Workshop](https://events.eclipse.org/2020/open-source-ai-workshop/) by creating an [issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/events.eclipse.org/-/issues/new?issuable_template=event).

The Eclipse Foundation Webdev team will start working on your request once the [Event Website Content Template](https://docs.google.com/document/d/1oVLBK8tzyuYC9OUisy1x-cc50PfGx0alnUHw9RYlZag) is completed and uploaded to your issue.

Please plan for a minimum of two weeks for the website implementation. Additional information is available in the [Event Website Content Template](https://docs.google.com/document/d/1oVLBK8tzyuYC9OUisy1x-cc50PfGx0alnUHw9RYlZag) document.

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [events.eclipse.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/events.eclipse.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/events.eclipse.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Related projects

### [EclipseFdn/solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/)

Hugo theme of the Eclipse Foundation look and feel.

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/events.eclipse.org/-/issues).


## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

**Angelika Wittek**

- <https://github.com/AngelikaWittek>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2018-2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [events.eclipse.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/events.eclipse.org/-/graphs/main). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/events.eclipse.org/-/raw/main/README.md).
