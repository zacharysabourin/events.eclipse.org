---
title: "eSAAM 2023 Topics"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2023 Topics</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        3rd Eclipse Security, AI, Architecture and Modelling Conference<br/>on Cloud to Edge Continuum
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center" style="color: white;">October 17, 2023 | Ludwigsburg, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2023-10-17T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-esaam2023-event"
hide_breadcrumb: true
show_featured_footer: false
hide_call_for_action: true
main_menu: esaam2023  
container: "container-fluid esaam-2023-event"
summary: "eSAAM 2023 will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, specifically  focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2023"
layout: single
keywords: ["eclipse", "UOM", "UPM", "conference", "research", "eSAAM", "SAAM", "Cloud", "Edge", "IoT", "Cloud-Edge-Continuum", "Edge-Cloud-Continuum", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling", "Registration"]
links: [ [href: "..", text: "Main Page"] ]
draft: false
---

{{< grid/section-container id="topics" class="featured-section-row">}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Security and Privacy for the Cloud to Edge Continuum
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Cloud Computing Security and Privacy](../images/security-black.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Security practices, and architectures
* Secure discovery and authentication
* Cloud-centric threat models
* Cloud cryptography 
* Cloud access control and key management 
* Secure computation outsourcing 
* Integrity and verifiable computation
* Computation of encrypted data
* Secure cloud resource virtualization mechanisms 
* Trusted computing technology 
* Failure and Vulnerability  detection and prediction
* Energy/cost/efficiency of security in clouds 
* Availability, recovery and auditing
* Network security mechanisms (e.g., IDS etc.) for the cloud 
* Security and Privacy of Federated Clouds and Edge/Fog Computing

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Artificial Intelligence for the Cloud to Edge Continuum
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Artificial Intelligence in Cloud Computing](../images/ai-black.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Intelligent distributed architectures and infrastructures
* Context-Awareness and Location- Awareness 
* Machine Learning and Deep Learning Approaches  
* AI, deep learning for predictive security
* Challenges, Use Cases, and Solutions for Industry and Society
* Federated learning in the cloud-edge continuum
* AI-powered resource allocation and energy optimization in the cloud
* Deploying AI models in the clouds: Tools, techniques and approaches
* Distributed training of deep learning models
* Large Language Models in the cloud
* Real-time processing with AI on the cloud
* AI-powered data management in the cloud
* Leveraging cloud for advancing transparency and explainability of AI models
* AI accelerators on the cloud (e.g., TPUs)

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Architectures for the Cloud to Edge Continuum
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Cloud Computing Architecture](../images/icon-architecture.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Cloud-oriented Architecture: Frameworks and Methodologies
* Cloud- and edge computing architectures for smart applications
* Microservices, monitoring and scalability of smart applications
* Service oriented architectures for smart applications
* Enterprise Cloud Computing Ecosystem: Technology, Architecture and Applications
* SaaS and Cloud Computing
* PaaS and IaaS Cloud Computing Service Models
* Serverless Cloud Computing Architecture and FaaS Model
* Building Cloud Computing Solutions at Scale
* The Cloud-to-Thing Continuum: Opportunities and Challenges 
* Green Cloud Computing Architecture
* Containerization and Cloud Deployment Model

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Modelling for the Cloud to Edge Continuum
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Cloud Computing Models and Services](../images/modeling-black.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Modelling Languages and Tools 
* Verification & Validation approaches
* Modelling for smart solutions
* Security & Privacy modelling
* Statistical models checking
* Modelling adaptive IoT systems 
* Runtime models
* Cloud programming and deployment models
* Power-aware profiling, modelling, and optimization for clouds
* Consistency, fault tolerance, and reliability models for clouds
* Cloud traffic characterization and measurements
* On-demand cloud computing models
* Challenges, Use Cases, and Solutions for Industry and Society
* Service-level agreements, business models, and pricing policies for cloud computing
* Open API and service orchestration on cloud platforms

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Images from
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* [Header by Roman from Pixabay](https://pixabay.com/illustrations/cloud-computer-circuit-board-cpu-6532831/)
* [Security and Privacy icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
* [Artificial Intelligence icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
* [Architecture icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
* [Models and Services icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)

{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}