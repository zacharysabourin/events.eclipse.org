---
title: Unlock the Cloud. Open New Markets.
summary: "Unlock the Cloud. Open new Markets | Event: April 25, 2023"
headline: Unlock the Cloud. Open new Markets.
subtitle: "" 
date: 2023-04-25T14:00:00+02:00
custom_jumbotron: |
    <div class="h2">Open Services Cloud <strong>In-Person Event</strong> | April 25, 2023 | Brussels</div>
    <a class="btn btn-primary margin-top-30" href="https://unlock-cloud.eventbrite.com/">REGISTER NOW</a>
    <div class="custom-jumbotron-details">
        <p>Join us at the Open Services Cloud Event and learn how cloud interoperability can boost the competitiveness and digital sovereignty of the EU.</p>
    </div>
    <div class="custom-jumbotron-img-wrapper">
        <img class="img img-responsive margin-auto" src="./images/unlock-the-cloud.png" alt="" />
    </div>
header_wrapper_class: featured-header container
custom_jumbotron_class: unlock-the-cloud-jumbotron
links: [[href: "#about", text: "ABOUT"], [href: "#agenda", text: "AGENDA"]]
container: container-fluid
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
hide_call_for_action: true
show_featured_footer: false
cascade:
    seo_title: Unlock the Cloud. Open New Markets. | OSC
    description: "Unlock the Cloud. Open new Markets | Event: April 25, 2023"
    keywords: ["cloud services", "open services cloud", "cloud computing", "digital sovereignty"]
    page_css_file: /css/2023-osc.css
    layout: single
---

<!-- About / Registration -->
{{< unlockthecloud/registration >}}

<!-- Speakers --> 
{{< grid/div id="speakers" class="container text-center eclipsefdn-user-display-circle" isMarkdown="false" >}}
    {{< events/user_display event="unlockthecloud" year="2023" title="Speakers" source="speakers" imageRoot="/2023/unlockthecloud/images/speakers/" displayLearnMore="false" subpage="speakers" >}}
    {{</ events/user_display >}}
{{</ grid/div >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="text-left row padding-top-40 padding-bottom-40" isMarkdown="false" >}}
    {{< unlockthecloud/agenda event="unlockthecloud" year="2023" >}}
    <small>* All times displayed are in Central European Summer Time (CEST).</small>
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

<!-- Call to Action section -->
{{< unlockthecloud/actions >}}
