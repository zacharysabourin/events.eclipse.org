---
logo: '2021/theiacon/images/logos/theiacon-logo.svg'
title: 'TheiaCon 2021'
seo_title: 'TheiaCon 2021'
headline: |
  <div class='headerCtn'><div class="rocketCtn"></div><h1>Leading the Next <br> Generation of Cloud <br> IDE Development</h1> <h2>Virtual Conference | November 17-18, 2021</h2></div>
tagline: ''
summary: 'TheiaCon is a virtual conference focused around the Eclipse Theia IDE ecosystem. It brings together a diverse group of Theia developers, adopters, and other contributors. The program will feature a mix of full-length talks, expert panel discussions and short ”lightning talks” focused on project insider, adopter, and broader ecosystem stories. This event is hosted by the Eclipse Foundation’s Cloud DevTools Working Group and is open to anyone interested in learning more about Cloud IDE development and the Theia project.'
date: 2021-11-17
header_wrapper_class: 'header-theiacon header-theiacon-event header-theiacon-event-2021'
links:
  [
    [href: '#committee', text: 'Program Committee'],
    [href: '#speakers', text: 'Speakers and Agenda'],
  ]
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
container: 'container-fluid'
layout: 'single'
---

{{< grid/div isMarkdown="false" class="row white-row" >}}
{{< grid/div isMarkdown="false" class="container theiacon-ctn" >}}
{{< events/registration event="theiacon" year="2021" />}}
{{</ grid/div >}}
{{</ grid/div >}}

<!-- Add user carousel for committee -->
{{< grid/section-container>}}
  {{< grid/div class="padding-top-40" id="committee" isMarkdown="false">}}
    {{< events/user_display event="theiacon" year="2021"  source="committee" imageRoot="/2021/theiacon/images/committee/" subpage="program-committee" >}}
    {{</ events/user_display >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Add user carousel for speakers -->
{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="theiacon" year="2021" title="Speakers" source="speakers" imageRoot="/2021/theiacon/images/committee/" displayLearnMore="false" subpage="speakers" />}}
{{</ grid/section-container >}}

<!-- Add agenda section -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg theiacon-2021" >}}
  {{< events/agenda event="theiacon" year="2021" >}}
{{</ grid/section-container >}}

<!-- Add sponsors section -->
{{< theiacon/sponsored-section class="text-center" display_sponsors="true" event="theiacon" year="2021" >}}
