---
title: "Speakers"
seo_title: "Jakarta One Livestream - Portuguese 2021 | Jakarta EE Software | Cloud Native"
date: 2021-10-27
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: "speakers"
---

{{< events/user_bios event="theiacon" year="2021" source="speakers" imgRoot="/2021/theiacon/images/committee/" >}}