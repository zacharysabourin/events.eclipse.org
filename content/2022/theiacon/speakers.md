---
title: "Speakers"
date: 2022-09-19T13:37:24-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: "speakers"
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

{{< events/user_bios event="theiacon" year="2022" source="speakers" imgRoot="/2022/theiacon/images/committee/" >}}