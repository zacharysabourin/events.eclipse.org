---
title: TheiaCon 2022
seo_title: TheiaCon 2022
date: 2022-11-30T10:00:00-04:00
logo: '2022/theiacon/images/logos/theiacon-logo.svg'
headline: ''
custom_jumbotron: |
    <div class="theiacon-jumbotron-2022">
        <div class="jumbotron-head">
            <div class="jumbotron-title-container">
                <h1 class="jumbotron-title">TheiaCon 2022</h1>
                <p class="jumbotron-subtitle">Leading the Next Generation of Cloud IDE Development</p>
                <p class="jumbotron-details-text">Virtual Conference | November 30 - December 1, 2022</p>
            </div>
            <div class="jumbotron-graphic-container">
                <div class="fifth-anniversary-icon-wrapper">
                  <img class="fifth-anniversary-icon" src="/images/2022/theiacon/theia-fifth-anniversary.png" alt="Fifth anniversary of Theia from 2017 to 2022" />
                </div>
            </div>
        </div>
    </div>
custom_jumbotron_class: container-fluid
header_wrapper_class: header-theiacon-event header-theiacon-event-2022
summary: 'TheiaCon is an annual virtual conference focused around the Eclipse Theia IDE ecosystem. It brings together a diverse group of Theia developers, adopters, and other contributors. The program will feature a mix of full-length talks, expert panel discussions and short lightning talks featuring project contributor insights, adopter stories, and work being done in the broader ecosystem.'
categories: []
keywords: ["TheiaCon", "TheiaCon 2022", "TheiaCon virtual conference", "virtual conference", "Theia developers conference", "Cloud DevTools Working Group"]
slug: ""
aliases: []
toc: false
draft: false
hide_page_title: true
hide_sidebar: true
container: container-fluid
layout: single
---
<!-- About and Registration -->
{{< grid/div isMarkdown="false" class="row white-row" >}}
  {{< grid/div id="about" isMarkdown="false" class="container theiacon-ctn " >}}
    {{< events/registration event="theiacon" year="2022" >}}
  {{</ grid/div >}}
{{</ grid/div >}}

<!-- Speakers -->
{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="theiacon" year="2022" title="Speakers" source="speakers" imageRoot="/2022/theiacon/images/committee/" displayLearnMore="false" subpage="speakers" />}}
{{</ grid/section-container >}}

<!-- Committee -->
{{< grid/section-container >}}
  {{< grid/div class="padding-top-40" id="committee" class="row padding-top-40" isMarkdown="false">}}
    {{< events/user_display event="theiacon" year="2022"  source="committee" imageRoot="/2022/theiacon/images/committee/" subpage="program-committee" >}}
    {{</ events/user_display >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="row padding-top-40 padding-bottom-40 theiacon-2021" isMarkdown="false" >}}
{{< events/agenda event="theiacon" year="2022" >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
{{</ grid/section-container >}}

<!-- Sponsors -->
{{< theiacon/sponsored-section class="text-center padding-bottom-40" working_group="cloud-development-tools" display_sponsors="true" year="2022" event="theiacon" >}}
