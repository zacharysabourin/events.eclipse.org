---
title: "Eclipse SAAM on Cloud 2022"
headline: "Eclipse SAAM on Cloud 2022"
subtitle: "Security, AI, Architecture and Modelling for next generation of edge-cloud computing continuum"
tagline: ""
date: 2022-10-25T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-saam-2022-event"
hide_breadcrumb: true
container: "container-fluid saam-2022-event"
summary: "The Eclipse SAAM on Cloud conference will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, especially focusing on Security and Privacy, Artificial Intelligence, Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2022"
layout: single
keywords: ["eclipse", "ATB", "CERTH", "ITI", "conference", "research", "SAAM", "Cloud", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: []
---

{{< grid/section-container id="organizing-committee" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="false">}}
<h2>CANCELED!</h2>
<h3 align="center">We regret to inform you that this conference has been canceled due to a lack of submissions. <br/>
We will try to find out why we had so few submissions, any feedback is welcome. <br/>
We hope to come back to you with a new iteration in 2023. <br/><br/>
Stay tuned!<h3>

{{</ grid/section-container >}}

<!-- Introduction -->

<!--
{{< grid/section-container id="registration" class="featured-section-row featured-section-row-light-bg">}}
{{< events/registration event="saam-cloud" year="2022" title="About the Event">}}
Mark your calendars for **Eclipse SAAM on Cloud 2022, October 25**. The conference will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, especially focusing on Security and Privacy, Artificial Intelligence, Architecture, Modelling and related challenges.


The call for papers is open. Download the [call for papers](cfp.pdf) now and talk with your colleagues about participating at Eclipse SAAM on Cloud 2022.

This year **Eclipse SAAM** is co-located with
[![EclipseCon 2022](images/logo-eclipsecon2022.png)](https://eclipsecon.org/2022)
{{</ events/registration >}}
{{</ grid/section-container >}}
-->


<!-- Topics -->
{{< grid/section-container id="topics" class="featured-section-row featured-section-row-highligthed-bg text-center">}}

<h2>Technical topics of interest in Cloud Computing</h2>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Cloud Computing Security and Privacy](images/security-black.png)](topics/index.html#cloud-computing-security-and-privacy)
### Security and Privacy
[Icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Artificial Intelligence in Cloud Computing](images/ai-black.png)](topics/index.html#artificial-intelligence-in-cloud-computing)
### Artificial Intelligence
[Icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Cloud Computing Architecture](images/icon-architecture.png)](topics/index.html#cloud-computing-architecture)
### Architecture
[Icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Cloud Computing Models and Services](images/modeling-black.png)](topics/index.html#modelling-for-cloud-systems-and-service)
### Models and Services
[Icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}


[//]: # (Dates)
<!--
{{< grid/section-container id="dates" class="featured-section-row featured-section-row-light-bg text-center">}}

<h2>Important Dates</h2>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}
![Paper submission deadline](images/date-09-05-old.png)

#### Paper Submission Deadline
{{</ grid/div >}}

{{< grid/div class="col-md-5 padding-bottom-20" isMarkdown="true">}}
![Paper submission extended deadline](images/date-09-15.png)

### Paper Submission Extended Deadline
{{</ grid/div >}}

{{< grid/div class="col-md-5 padding-bottom-20" isMarkdown="true">}}
![Acceptance Notification](images/date-10-03.png)

### Acceptance Notification
{{</ grid/div >}}

{{< grid/div class="col-md-5 padding-bottom-20" isMarkdown="true">}}
![Camera-Ready Paper Submission](images/date-10-18.png)

### Camera-Ready Paper Submission
{{</ grid/div >}}

{{< grid/div class="col-md-5 padding-bottom-20" isMarkdown="true">}}
![Conference Dates](images/date-10-25.png)

## Conference Date

{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}
-->

<!-- Proceedings -->
<!--
{{< grid/section-container id="proceedings" class="featured-section-row featured-section-row-lighter-bg text-center">}}
	<h2>Proceedings</h2>
	<h4>The papers will be peer reviewed. Selected papers will be published under the <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4 .0</a> license on <a href="http://ceur-ws.org">the CEUR portal</a>.</h4>
    <hr/>
    <h3>Indexed by:</h3>
{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![Scopus](images/scopus_logo.png)](https://www.scopus.com/)
{{</ grid/div >}}
 
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![Google Scholar](images/GScholar_logo.png)](https://scholar.google.com/)
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![dblp](images/dblp_logo.png)](https://dblp.org)
{{</ grid/div >}}
 
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
  [![Semantic Scholar](images/semantic_scholar_logo.png)](https://www.semanticscholar.org)
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}
-->

[//]: # (Agenda)
<!--
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="saam-cloud" year="2022" >}}
{{</ grid/section-container >}}
-->
[//]: # (TCP)

<!--
{{< grid/section-container id="program-committee" class="featured-section-row featured-section-row-lighter-bg">}}
<h2>Technical Program Committee</h2>

<p>The Technical Program Committee is an independent panel of expert volunteers and as such will do their best to judge papers objectively and on the principle of a level playing field for all. </p>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Alessandra Bagnato, Softeam, France
* Alexandros Chatzigeorgiou, UoM, Greece
* Athanasios Salamanis, CERTH/ITI, Greece
* Benoit Combemale, INRIA, France
* Dimitrios Tsoukalas, CERTH/ITI, Greece
* Dimitris Syrivellis, NVIDIA, Israel
* Enrico Ferrera, LINKS Foundation, Italy
* Evi Karakozoglou, Google Cloud, UK
* Fulya Horozal, ATB, Germany
{{</ grid/div >}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Jean-Yves Rigolet, IBM, France
* Jose Barata, FCT NOVA, Portugal
* Kevin Nagorny, ATB, Germany
* Marco Jahn, Eclipse Foundation, Germany
* Maria-Teresa Delgado, Eclipse Foundation, Mexico
* Marija Jankovic, CERTH/ITI, Greece
* Miltiadis Siavvas, CERTH/ITI, Greece
* Teemu Karvonen, University of Oulu, Finland
* Tero Päivärinta, University of Oulu, Finland
{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}
-->

[//]: # (Speakers)
<!--
{{< grid/section-container id="keynote" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="saam-cloud" year="2022" title="Keynotes Speakers" source="keynote" imageRoot="/2022/saam-cloud/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}
-->

[//]: # (Organizing Committee)
{{< grid/section-container id="organizing-committee" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="true">}}

## Organizing Committee 

The Eclipse SAAM on Cloud 2022 conference is co-organized by the Eclipse Foundation, ATB and the CERTH.

### Conference Co-chairs
Sebastian Scholze, ATB, Germany\
Dionysios Kehagias, CERTH/ITI, Greece\
Philippe Krief, Eclipse Foundation, France

### Program Committee Chair
Fulya Horozal, ATB, Germany\
Marija Jankovic, CERTH/ITI, Greece\
Marco Jahn, Eclipse Foundation, Germany

### Publicity  
Silvia Miles, Eclipse Foundation Europe, Germany
{{</ grid/section-container >}}

{{< grid/section-container id="organizers" class="featured-section-row featured-section-row-lighter-bg text-center">}}
  {{< events/sponsors event="saam-cloud" year="2022" source="coorganizers" title="Co-organizers" useMax="false" displayBecomeSponsor="false">}}
{{</ grid/section-container >}}

{{< grid/section-container id="sponsors" class="featured-section-row featured-section-row-light-bg text-center">}}
  {{< events/sponsors event="saam-cloud" year="2022" source="sponsors" title="Sponsors" useMax="false" displayBecomeSponsor="false">}}
{{</ grid/section-container >}}


